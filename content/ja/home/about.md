---
title: "About"
toc: true
---

## 基礎情報

<table>
<tbody>
<tr><td>Email</td><td><a href="mailto:fj68forg+career@gmail.com">fj68forg+career@gmail.com</a></td></tr>
<tr><td>居住地</td><td>栃木県</td></tr>
<tr><td>年齢</td><td>25歳（2021年3月13日現在）</td></tr>
</tbody>
</table>

## SNS

<table>
<tbody>
<tr><td>Linktree</td><td><a href="https://linktr.ee/fj68">linktr.ee/fj68</a></td><td></td></tr>
<tr><td>GitLab</td><td><a href="gitlab.com/fj68">gitlab.com/fj68</a></td><td>メイン</td></tr>
<tr><td>GitHub</td><td><a href="github.com/fj68">github.com/fj68</a></td><td>昔のもの</td></tr>
<tr><td>Paiza</td><td>Nickname: fj68</td><td>Rate: 1544（2021年10月1日現在）</td></tr>
</tbody>
</table>

## ソフトウェア等

| 名前 | 詳細ページ | 説明 | 使用した技術スタック |
| ---- | ------------ | ----------- | ----- |
| md-dev-server | [gitlab.com/fj68/md-dev-server](gitlab.com/fj68/md-dev-server) | Markdown専用のlive-reloadサーバ | Golang |
| go-after | [gitlab.com/fj68/go-after](gitlab.com/fj68/) | シンプルな CLI タイマー | Golang |

## 職歴

| 期間                  | 名前 | 部署・役職 | 雇用形態 |
| --------------------- | ---- | ---------- | -------- |
| 2016年3月〜2017年7月  | 株式会社コンベンション・リンケージ | 府中市生涯学習センター（総合受付） | アルバイト |
| 2018年3月〜2018年5月  | CloudWorksのクラウドワーカー | N/A | クラウドワーカー |
| 2018年5月〜2019年3月  | オリジン東秀株式会社 | キッチン・接客スタッフ | アルバイト |
| 2019年12月〜2021年3月 | 在日本某国大使館 | 秘書のアシスタント | アルバイト |

## 学歴

| 期間                  | 名前 | 学部・学科 | 専攻 |
| --------------------- | ---- | ---------- | ----- |
| 2002年4月〜2008年3月  | 宇都宮市立宮の原小学校 | N/A | N/A |
| 2008年4月〜2011年3月  | 宇都宮市立宮の原中学校 | N/A | N/A |
| 2011年4月〜2014年3月  | 栃木県立宇都宮高等学校 | 普通科 | N/A |
| 2014年4月〜2021年10月 | 東京外国語大学 中途退学 | 言語文化学部 | 言語学（モンゴル語） |

## スキル

### 言語

母語は日本語で、英語も日常会話程度なら可能。

 - 英検 準2級（2010年）
 - TOEIC Listening & Reading Test Score: 945（2017年）
 - 在日本某国大使館にて英語を使いながら勤務

#### 英語ディベート

 - 2012年に「Utsunomiya」チームとして第7回全国高校生英語ディベート大会で優勝。
 - 2013年に日本代表チームとして高校生ディベート世界大会に出場。
 - 2015年に「Tokyo UFS A」チームとして大学生ディベート世界大会に出場。
 - 2015年に日本ディベート連盟公式大会 Pre-Australs 2015 にてタブ・ディレクター補を務める。
 - 2016年に日本で唯一の大学生国際大会である第25回 ICU トーナメントにおいてタブ・ディレクターを務める。
 - 2017年に第26回 ICU トーナメントにおいてタブ・ディレクターを務める。

他、選手 / 審判 / 運営としての豊富な経験がある。

### Webフロントエンド

HTML 4.01の頃（JQueryすらなく、DHTMLなどと呼ばれていた時代）から HTML Application (HTA) を含め多くのコードを書いてきました。
DOM, Web Components, Web WorkersなどのWeb APIには慣れ親しんでいます。

最近はWeb AssemblyやBonsaiというOCamlでブラウザベースのインタラクティブなUIを作れるライブラリに興味を持っています。

 - HTML / CSS / Stylus / JavaScript / TypeScript / Elm
 - JQuery / Vue / React / hyperapp / Lit
 - Node.js eco-system / gulp / babel / webpack / parcel
 - Electron
 - Phaser 3 / pixi.js

### OCaml

私のお気に入りの言語です。よくアイデアをプロトタイプするのに使います。

前述の通り、最近はBonsaiに興味を持っています。

 - ocamlbuild / dune / OPAM
 - Core / Lwt / Angstrom / Opium

### Golang

CLIツールやバックエンドサーバを作る時のファーストチョイスです。
Cgoを使ってDLLをクロスコンパイルしたこともあります。

### Python

Python 2.6の頃から書いていますが、新しく書くコードはPython 3でかつmypy（静的型検査）も一緒だと嬉しいです。

 - Django / flask / bottle
 - Pygame / PySDL2 / pyglet

### C

BCC (Borland C Compiler), gcc, clangを使っていました。

C++に関してはさほど経験がありませんが、とにかく言えることは「読み書きはできるけれど仕様を完全に理解するのは不可能だ」ということです。

### Lua

Luaのコードを書くことも、Cなどの言語に組み込むことも可能です。

### Krkr

TJS2やKAG3の経験があります。

### HSP

中学生の頃によく使用しており、HSPを通してWindows APIの使い方を学びました。

## 主な活動

### 英語ディベート

高校の「英語部」メンバーとして活動を始め、その後大学の TUFS ESS: Debate Section に移る。

### 茶道

7歳の頃に茶道のお稽古を始める。 10歳〜17歳の間はやむを得ない事情により離れていたが、大学生になって新たな先生の元に入門し、再開した。

どのように美味しいお茶をお出しするかということだけでなく、和の心についても学んでいる。

### コンピュータとプログラミング

9歳の頃にプログラミングとコンピュータに出会う。

高校生になるまでは Windows 98 SE が自分のメイン・コンピュータで、一番好きな OS は Windows 2000 だった。 私にとって初めての Linux ディストリビューションである Slack を USB メモリにインストールした時、Windows が唯一の OS ではないことに気付く。

Windows 3.x (オリジナルの MS-DOS 含む), Windows Me そして Windows NT 以外のすべての Microsoft Windows シリーズを 1年以上使用した経験がある。 Linux ディストリビューションに関しては、Ubuntu, Slack, Puppy Linux そして CentOS のユーザーだった。

現在のメインPCはMacBook Pro mid 2012で、友人が譲ってくれたものにLinux Mint Xfce Edition をインストールして使用している。
大使館ではMachintoshを主に使用していた。

何かを直すことが趣味なので、新しいものよりも古くて安いジャンク品を好んでいる。

プログラミング言語は好きなもののうちの一つであり、特に静的型検査・型推論がある言語が好みである。
HTML 4.01 と CSS 2 を最初に経験し、JavaScript 1.x（おそらく 1.5）が一番好きだった。
それから、HSP, C/C++, アセンブリ (NASM), Tcl/Tk, NScripter, TJS/KAG, CatSystem2, Lua, C#, Python, Kotlin, Haxe/Neko, OCaml, Elm, Go, TypeScript ...etc. で遊ぶ。
プログラミング言語自体が好きなため、新しい言語を学ぶのは非常に楽しく感じている。

### 温泉と銭湯

風呂好き。 そのため、大学で「銭湯同好会」(a.k.a. TUFSen) のメンバーになったのは自然な流れである。

1年半ほど会計担当を努めていた間に 2回の銭湯合宿、大学の学校祭である外語祭での展示（学長賞を受賞）、半年に 1回の銭湯ツアー、そして月毎の銭湯探訪といったイベントを企画・開催した。
また、週ごとに行っていた運営会議に加えて銭湯総会を半年に 1回開催するようになった。

現在は引退しているが、風呂好きは変わらず健在。

特に好きな温泉は鹿の湯、酸ヶ湯、万座温泉、古遠部温泉などの硫黄泉・塩化物泉。 基本的に素泊まり。

### その他

 - 読書（小説とエッセイ）
 - 映画 / アニメの鑑賞
 - 歌を歌うこと
 - ピアノを弾くこと（まだ上手ではありません）
 - 料理
 - カメラと写真（Pentax K-5 と smc PENTAX DA 18-55mm F3.5-5.6AL WRを使っています）
 - ディズニー（特に音楽とアトラクション、BGS）
 - 地形や歴史を感じながら街歩きすること
 
