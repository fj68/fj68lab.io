---
title: "About"
toc: true
---

## Personal data

<table>
<tbody>
<tr><td>Email</td><td><a href="mailto:fj68forg+career@gmail.com">fj68forg+career@gmail.com</a></td></tr>
<tr><td>Country</td><td>Tochigi, Japan</td></tr>
<tr><td>Age</td><td>25 as of 13 Mar. 2021</td></tr>
<tr><td>Pronouns</td><td>He / Him</td></tr>
</tbody>
</table>

## Socials

<table>
<tbody>
<tr><td>Linktree</td><td><a href="https://linktr.ee/fj68">linktr.ee/fj68</a></td><td></td></tr>
<tr><td>GitLab</td><td><a href="gitlab.com/fj68">gitlab.com/fj68</a></td><td>main</td></tr>
<tr><td>GitHub</td><td><a href="github.com/fj68">github.com/fj68</a></td><td>old</td></tr>
<tr><td>Paiza</td><td>Nickname: fj68</td><td>Rate: 1544 as of 1 Oct. 2021</td></tr>
</tbody>
</table>

## Works

| Name | Project Page | Description | Using |
| ---- | ------------ | ----------- | ----- |
| md-dev-server | [gitlab.com/fj68/md-dev-server](gitlab.com/fj68/md-dev-server) | Live-reload server for markdown | Golang |
| go-after | [gitlab.com/fj68/go-after](gitlab.com/fj68/) | Simple CLI timer | Golang |

## Career history

| Since     | Until     | Organization | Division / Title | Status |
| --------- | --------- | ------------ | ---------------- | ------ |
| Dec. 2019 | Mar. 2021 | A certain embassy | Secretary Assistant | Part-time |
| May. 2018 | Mar. 2019 | Origin Toshu Co., Ltd. | Kitchen / Floor stuff | Part-time |
| Mar. 2018 | May. 2018 | Cloud worker on CloudWorks | N/A | Cloud worker |
| Mar. 2016 | Jul. 2017 | CONVENTION LINKAGE, INC. | Fuchu City Lifelong Study Center (General Information Counter) | Part-time |

## Education

| Since     | Until     | Name | Course / Faculty | Major |
| --------- | --------- | ---- | ---------------- | ----- |
| Apr. 2014 | Oct. 2021 | Tokyo University of Foreign Studies (Withdrawal) | Language and Culture | Linguistics (Mongolian) |
| Apr. 2011 | Mar. 2014 | Utsunomiya High School | General course | N/A |
| Apr. 2008 | Mar. 2011 | Miyanohara Junior High School | N/A | N/A |
| Apr. 2002 | Mar. 2008 | Miyanohara Elementary School | N/A | N/A |

## Skills

### Languages

While my mother tongue is Japanese, I can use English too.

 - The EIKEN Test in Practical English Proficiency: Grade Pre-2 in 2010
 - TOEIC Listening & Reading Test Score: 945 in 2017
 - Worked at certain embassy using English

#### English Debate

 - The Champion of the 7th All Japan High School English Debate Tournament in 2012, as Utsunomiya team.
 - Participated in the World Schools Debating Championships (WSDC) in 2013 as Japanese national team.
 - Participated in the World University Debating Championships (WUDC) in 2015 as Tokyo UFS A team.
 - Vice Tabulation Director of JPDU Pre-Australs 2015.
 - Tabulation Director of the 25th ICU Tournament in 2016 (ICUT is a international tournament).
 - Tabulation Director of the 26th ICU Tournament in 2017.

And many other experiences as a Debater / Adjudicator / Organizing Committee.

### Web frontend

Since HTML 4.01 (the era even without JQuery and so-called DHTML), I wrote a lot of frontend codes including HTML Application (a.k.a. HTA).
I'm familiar with Web APIs including DOM, Web Components, Web Workers.

Recently I'm interested in Web Assembly and Bonsai (OCaml library for building interactive browser-based UI).

 - HTML / CSS / Stylus / JavaScript / TypeScript / Elm
 - JQuery / Vue / React / hyperapp / Lit
 - Node.js eco-system / gulp / babel / webpack / parcel
 - Electron
 - Phaser 3 / pixi.js

### OCaml

My favorite language. I often use OCaml to write my idea out as a prototype.

As mentioned above, I'm interested in Bonsai recently.

 - ocamlbuild / dune / OPAM
 - Core / Lwt / Angstrom / Opium

### Golang

My first choice to create CLI tools and backend servers. Also I have an experience to write a DLL with cgo, cross-compiling from Linux to Windows.

### Python

From Python 2.6, but I'd like to write new python applications in Python 3 with mypy (static type checker).

 - Django / flask / bottle
 - Pygame / PySDL2 / pyglet

### C

I used bcc (Borland C Compiler), gcc and clang.

As for C++, not much experiences and what I can say is "able to read and use it as a better C, but unable to 'fully-understand' its specs."

### Lua

Both writing lua codes and embedding lua in C or other familiar languages are possible.

### Krkr

Experience with TJS2, KAG3.

### HSP

Heavily used when I was junior high student and I learned about Windows API through HSP.

## Activities

### English Debate

Started as a member of English Club in my high school, then move to TUFS ESS: Debate Section in my university.

### Sa-do (Japanese Traditional Art of Tea)

From age 7, I’ ve practiced 茶道 (Sa-do, [Wikipedia](https://en.wikipedia.org/wiki/Japanese_tea_ceremony)). Though it had suspended during age 10-17 due to circumstances beyond my control, it resumed when I became a university student in 2014.

Not only studying how to make a delicious cup of tea, I've learned the heart of 和 (Wa, peace and harmony).

### Programming & Computers

Until I became a high school student, Windows 98 SE is my main computer and my best favorite OS was Windows 2000. After installing Slack, my fiirst Linux Distro into the USB memory, I noticed that Windows is not the only available OS.

I have touched all versions of Microsoft Windows series except Windows 3.x (including original MS-DOS), Windows Me and Windows NT at least for a year. As for Linux Distros, I was a user of Ubuntu, Slack, Puppy Linux and CentOS.

My main computer is MacBook Pro mid 2012 which my friend gave me and I installed Linux Mint Xfce Edition.
Also in the embassy, we used Machintosh mainly.

I prefer old and junk device to new and packaged one, because of my hobby to repair something.

Programming language is one of my favorites, especially statically typed and type inferred languages.

As for it, HTML 4.01 and CSS 2 was my initial experience and my favorite was JavaScript 1.x (1.5, maybe).
Since then, I played with HSP, C/C++, Assembly (NASM), Tcl/Tk, NScripter, TJS/KAG, CatSystem2, Lua, C#, Python, Kotlin, Haxe/Neko, OCaml, Elm, Go, TypeScript ...etc.

Because I love programming languages itself, learning new programming languages are very fun!

### Onsen & Sento (Hot Spring & Japanese Traditional Public Bath)

I love bath!
Thus it is quite natural to become a primary member of Sento Society (a.k.a. TUFSen) in my university.

While I experinced the Financial Director for almost 1.5 years there, we hold our 1st and 2nd Sento Training Camp of the Year, exhibitions at school festival of my university (which got President's award), Sento-Tour events per half the year and Monthly Sento Visits events.
Also, we started General Meeting per half a year, in adddition to Weekly Meeting of Boards.

Now I'm a ex-member, but bath still attraccts me.

My favorite Onsens are sulfur springs like Shika no Yu, Suka Yu, Manza Onsen or Furutobe Onsen.

### Others

- Reading Books (Novels & Essays)
- Watching Movies & Animes
- Singing Songs
- Playing Piano (not well yet)
- Cooking
- Camera & Photo (I use Pentax K-5 & smc PENTAX DA 18-55mm F3.5-5.6AL WR)
- Disney (especially, Musics, Attractions and BGS)
- Walking around the city, feeling geographical features and history of the area
